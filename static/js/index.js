$('#list-tab a').on('click', function (e) {
    e.preventDefault()
    $(this).tab('show')
})
$('#addPersonModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('org') // Extract info from data-* attributes
    var modal = $(this)
    modal.find('#companyNamePerson').val(recipient)
})
$('#editPersonModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('org') // Extract info from data-* attributes
    var name = button.data('name') // Extract info from data-* attributes
    var address = button.data('address') // Extract info from data-* attributes
    var postcode = button.data('postcode') // Extract info from data-* attributes
    var tel = button.data('tel') // Extract info from data-* attributes
    var modal = $(this)
    modal.find('#editCompanyNamePerson').val(recipient)
    modal.find('#editPersonNameOriginal').val(name)
    modal.find('#editPersonName').val(name)
    modal.find('#editPersonAddress').val(address)
    modal.find('#editPersonPostcode').val(postcode)
    modal.find('#editPersonTel').val(tel)
})
$('#editCompanyModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var name = button.data('name') // Extract info from data-* attributes
    var address = button.data('address') // Extract info from data-* attributes
    var postcode = button.data('postcode') // Extract info from data-* attributes
    var tel = button.data('tel') // Extract info from data-* attributes
    var modal = $(this)
    modal.find('#editCompanyOriginal').val(name)
    modal.find('#editCompanyName').val(name)
    modal.find('#editCompanyAddress').val(address)
    modal.find('#editCompanyPostcode').val(postcode)
    modal.find('#editCompanyTel').val(tel)
})
$('#delCompanyModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var name = button.data('name') // Extract info from data-* attributes
    var modal = $(this)
    modal.find('#delCompanyName').val(name)
    modal.find('#delCompany').text(name)
})
$('#delPersonModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('org') // Extract info from data-* attributes
    var name = button.data('name') // Extract info from data-* attributes
    var modal = $(this)
    modal.find('#delCompanyNamePerson').val(recipient)
    modal.find('#delPersonName').val(name)
    modal.find('#delPerson').text(name)
})
