import json

from flask import Flask, render_template, request, redirect, url_for

app = Flask(__name__)
# TODO: Check for file
try:
    with open("contacts.json", "r") as f:
        data = json.load(f)
        contacts = data["organisations"]
except FileNotFoundError as e:
    contacts = []


@app.route("/")
def index():
    return render_template("index.html", contacts=contacts)


@app.route("/add/company", methods=['POST'])
def add_company():
    contacts.append({"name": request.form['companyName'],
                     "address": request.form['companyAddress'],
                     "postcode": request.form['companyPostcode'],
                     "tel": request.form['companyTel'],
                     "people": []
                     })
    with open("contacts.json", "w") as new_data:
        json.dump({"organisations": contacts}, new_data)
    return redirect(url_for("index"))


@app.route("/add/person", methods=['POST'])
def add_person():
    print(contacts)
    for org in contacts:
        if org["name"] == request.form["companyNamePerson"]:
            org["people"].append({"name": request.form['personName'],
                                  "address": request.form['personAddress'],
                                  "postcode": request.form['personPostcode'],
                                  "tel": request.form['personTel'],
                                  })

    print(contacts)
    with open("contacts.json", "w") as new_data:
        json.dump({"organisations": contacts}, new_data)
    return redirect(url_for("index"))


@app.route("/edit/company", methods=['POST'])
def edit_company():
    for org in contacts:
        if org["name"] == request.form["editCompanyOriginal"]:
            org["name"] = request.form["editCompanyName"]
            org["address"] = request.form["editCompanyAddress"]
            org["postcode"] = request.form["editCompanyPostcode"]
            org["tel"] = request.form["editCompanyTel"]
    with open("contacts.json", "w") as new_data:
        json.dump({"organisations": contacts}, new_data)
    return redirect(url_for("index"))


@app.route("/edit/person", methods=['POST'])
def edit_person():
    for org in contacts:
        if org["name"] == request.form["editCompanyNamePerson"]:
            for person in org["people"]:
                if person["name"] == request.form["editPersonNameOriginal"]:
                    org["people"].remove(person)
                    org["people"].append({"name": request.form['editPersonName'],
                                          "address": request.form['editPersonAddress'],
                                          "postcode": request.form['editPersonPostcode'],
                                          "tel": request.form['editPersonTel'],
                                          })

    with open("contacts.json", "w") as new_data:
        json.dump({"organisations": contacts}, new_data)
    return redirect(url_for("index"))


@app.route("/delete/company", methods=['POST'])
def del_company():
    for org in contacts:
        if org["name"] == request.form["delCompanyName"]:
            contacts.remove(org)
    with open("contacts.json", "w") as new_data:
        json.dump({"organisations": contacts}, new_data)
    return redirect(url_for("index"))


@app.route("/delete/person", methods=['POST'])
def del_person():
    for org in contacts:
        if org["name"] == request.form["delCompanyNamePerson"]:
            for person in org["people"]:
                if person["name"] == request.form["delPersonName"]:
                    org["people"].remove(person)

    with open("contacts.json", "w") as new_data:
        json.dump({"organisations": contacts}, new_data)
    return redirect(url_for("index"))


if __name__ == "__main__":
    app.run()
